#include <stdio.h>
#include <stdlib.h>
#include <string.h> // para los parametros
#include <sys/types.h> // para el handler
#include<time.h> // para srand
#include "cabecera.h"
#define LEER 	 0
#define ESCRIBIR 1

extern int senales_recibidas;//la variable global que he creado en el main tambien la pueda utilizar aqui

int numeros_iguales(int n,int *num)// que no haya numeros iguales en la apuesta
{
	int i;
	    
	for(i=0; i<6; i++)
	{
		if(n == num[i])
		{
			return 1;
		} 
				
	}

    return 0;    
}

int* apuesta()
{
srand(getpid()+14234234234); //para que cada combinacion del hijo sea diferente
    int  aux,k,i,n;
	int *num=(int*)malloc(6*sizeof(int));

	for(k=0; k<6; k++)//Inicializo el vector a 0
	{
		num[k] = 0;
	}

    for( i=0; i<6; i++)
    {
        do
		{
            n = 1 + rand() % 15;// generacion de los numeros aleatorios

        }while(numeros_iguales(n,num) == 1);
        num[i] = n;
    }


    for(n=0; n<6; n++)
    {
        for(i=n+1; i<6; i++)// i=n+1 es porque como estamos comparando dos elementos del vector V[0] y V[1]
        {                    //uno tiene que ir por delante del otro para no comparar la misma casilla del vector

            if(num[n]>num[i])//metodo de la burbuja
            {
                aux=num[n];
                num[n]=num[i];
                num[i]=aux;
            }
        }

    }

	return num;
}




int* combinacion_ganadora()
{
srand(getpid()+29879797987); //para que cada combinacion del hijo sea diferente
    int  aux,k,i,n;
	int *num=(int*)malloc(6*sizeof(int));//num es numero que genera la combinacion ganadora"y apuesta"

	for(k=0; k<6; k++)//Inicializo el vector a 0
	{
		num[k] = 0;
	}

    for( i=0; i<6; i++)
    {
        do
		{
            n = 1 + rand() % 15;// generacion de los numeros aleatorios

        }while(numeros_iguales(n,num) == 1);
        num[i] = n;
    }


    for(n=0; n<6; n++)//metodo de la burbuja
    {
        for(i=n+1; i<6; i++)// i=n+1 es porque como estamos comparando dos elementos del vector V[0] y V[1]
        {                    //uno tiene que ir por delante del otro para no comparar la misma casilla del vector

            if(num[n]>num[i])
            {
                aux=num[n];
                num[n]=num[i];
                num[i]=aux;
            }
        }

    }

	return num;
}



void mostrar_combinacion_ganadora(int *num)
{
	int i;

	for(i=0; i<6; i++)
	{
	  printf("%d ",num[i]);	
	}
	printf("\n");

}




void mostrar_apuesta(int *num)
{
	int i;

	for(i=0; i<6; i++)
	{
	  printf("%d ",num[i]);	
	}
	printf("\n");

}


void handler (int signo, siginfo_t *info, void *other) //manejador
{
char *src;

	switch(info->si_code) 
	{
		case SI_QUEUE:
			src = "sigqueue";
			senales_recibidas++;// incremento de las señales enviadas del padre 
			break;
		case SI_TIMER: src = "timer"; break;
		case SI_ASYNCIO: src = "asyncio"; break;
		case SI_MESGQ: src = "msg queue"; break;
		case SI_USER: src = "kill/abort/raise"; break;
		default: src = "unknown!"; break;
	}

	//printf("Signal %d: source = %s, data = %d\n",signo, src, info->si_value.sival_int);
}

void padre_escribe(int **vector_pipes,int jugadores, int *combinacion)
{
	int i;
	char pasar_combinacion[6];
	for(i=0; i<6; i++)
    {
       pasar_combinacion[i] = combinacion[i] + '0'; // le sumo el caracter '0' de la tabla ASCII
    }
	for(i=0; i<jugadores; i++)
	{
	   close (vector_pipes[i][LEER]);
       write (vector_pipes[i][ESCRIBIR], pasar_combinacion, strlen(pasar_combinacion));
       close (vector_pipes[i][ESCRIBIR]);
	}
}

int* hijo_lee(int *vector_pipes)
{
       
	    int i,*combinacion=(int*)malloc(6*sizeof(int));
		char mensaje[6];

	  
		   close (vector_pipes[ESCRIBIR]);

			   read (vector_pipes[LEER], mensaje, 100);
		 	   
			   for(i=0; i<6; i++)
			   {
				   combinacion[i] = mensaje[i] - '0'; // le resto el caracter '0' de la tabla ASCII
			   }
				
			   
	   	   close (vector_pipes[LEER]);

			  return combinacion;
}



int comparar_resultados(int* combinacion_hijo , int *apuestaHijo)
{
	int i,j;
	int aciertos=0;

	for(i=0; i<6; i++)
	{
		for(j=0; j<6; j++)
		{
			if(combinacion_hijo[i] == apuestaHijo[j])
			{
				aciertos++;
			}
		}
	}
	return aciertos;
}


int hijo_ganancia(int salida)
{
  if(salida<3)  return 0;
  if(salida==3) return 10;
  if(salida==4) return 50;
  if(salida==5) return 500;
  if(salida==6) return 10000;
}




// ver que hijo a devuelto  los aciertos de su apuesta "exit"
int hijo_premiado(int pid_devuelto,int salida,int jugadores,HIJO* hijo)
{
	int i;
	for(i=0; i<jugadores; i++)
	{	
		if(pid_devuelto == hijo[i].pid)
		{		
		 hijo[i].premio=hijo_ganancia(salida>>8);
		 return hijo[i].num;	
		}
	}
}

void generar_fichero(int jugadores,int sorteos,HIJO* hijo)
{
    int i,j;
    FILE *archivo; // crea un puntero de tipo FILE
    char nombre_archivo[30]; // nombre del fichero con un limite de 30 caracteres
	char imprimir_linea[30]; // es para el titulo del fichero
	char resultados_finales[30];



         sprintf(nombre_archivo,"S%dR", sorteos);// genera el nombre de cada archivo
         archivo = fopen(nombre_archivo,"wt"); // "w": abrir un archivo para escritura, se crea si no existe o se sobreescribe si 												  //	existe.
                                                       
        if(archivo==NULL)
        {
            printf("Error en crear el archivo\n");
        }

		

		sprintf(imprimir_linea,"#Resultados sorteo %d\n",sorteos);
		fputs(imprimir_linea , archivo);

		for(j=0; j<jugadores; j++)
		{
			sprintf(resultados_finales,"%d\n",hijo[j].premio);
			fputs(resultados_finales , archivo);	
		}

		

        fclose (archivo);// despues de cada escritura o lectura el fichero debera de cerrarse
  
}

























