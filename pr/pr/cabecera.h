// CABEZERA DEL PROGRAMA SORTEO
#include <signal.h>

typedef struct
{
	int pid; // PID del hijo
	int num; // numero de orden del hijo
	int premio;//premio del hijo en un sorteo
}HIJO;

struct sigaction sa;
union sigval v;


int* apuesta(); // funcion generacion de apuesta 

int* combinacion_ganadora(); // funcion generacion de la combinacion ganadora

int numeros_iguales(int n, int *num);// funcion que me cambia los numeros iguales

void handler (int signo, siginfo_t *info, void *other); // funcion del manejador que controla las señales de tiempo real

//PARTE DE PIPE's
void padre_escribe(int **vector_pipes,int jugadores,int *combinacion);// padre ESCRIBE  en el PIPE
int* hijo_lee(int *vector_pipes);//el hijo LEE del PIPE

int comparar_resultados(int* combinacion_hijo , int* apuestaHijo);// el hijo compara su apuesta con la combinacion ganadora

int hijo_ganancia(int salida);// numeros de aciertos del hijo pasados a €

int hijo_premiado(int pid_devuelto,int salida,int jugadores,HIJO* hijo);// funcion para ver que hijo lo que ha ganado

void generar_fichero(int jugadores,int sorteos,HIJO* hijo); // generar el fichero con la ganancia optenida de cada jugador

//void mostrar_apuesta(int *num);
//void mostrar_combinacion_ganadora(int *num);
