/*############################################################################
# Nombre de la practica: Loteria (6/15)										 #
# NIE: X-5720766-E															 #
# Realizada por: Nestor Dobrinov Edrev										 #
# Curso: 2º Grado de ingenieria informatica en tecnologias de la informacion #
# Asignatura: Sistemas Operativos										     #
# Fecha: 18/01/2014														     #
############################################################################## */



#include <stdio.h> // estandar entrada/salida
#include <stdlib.h>
#include <string.h> // para los parametros
#include <sys/types.h> //es para la funcion getpid();
#include <sys/wait.h> //
#include <signal.h>
#include <unistd.h>// libreria para los pipe's. // 1 escribe, 0 lee
#include "cabecera.h"

#define LEER 	 0
#define ESCRIBIR 1

int senales_recibidas=0;

int main (int argc , char *argv[])
{
	

	sa.sa_sigaction = handler;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGRTMIN,&sa,NULL);

	
	pid_t pid; // declaracion de un proceso

	int i,jugadores,sorteos;
	int *apuestaHijo;
	int *combinacion;
	
	
	sorteos=atoi(argv[1]);	 // parametro 1
	jugadores=atoi(argv[2]); // parametro 2

	HIJO *hijo;
	hijo = (HIJO*)malloc(jugadores*sizeof(HIJO));
	
	
	if(sorteos<1 || sorteos>15)
	{
		printf("Error a la hora de introducir el parametro de sorteos. min: 1 y max: 15\n");
		return -1;
	}

	if(jugadores<1 || jugadores>10)
	{
		printf("Error a la hora de introducir el parametro de jugadores. min: 1 y max: 10\n");
		return -1;
	}

	if(argc != 3)// mira aver si estan todos los parametos con argc 
	{
		printf("Error a la hora de introducir el numero de parametros.Deben de ser 2\n");
		return -1;
	}




	int **vector_pipes=(int**)malloc(jugadores * sizeof(int*));//reservar memoria dinamica de doble puntero a enteros 
	for(i=0; i<jugadores; i++)								 //siendo el tamaño del vector los numeros de jugadores				
	{
		vector_pipes[i]=(int*)malloc(2 * sizeof(int)); //reserva para los pipe
	}		

	for(i=0; i<jugadores; i++)
	{
		if(pipe(vector_pipes[i]) == -1)
		{
		  printf("ERRORR al crear el pipe %d",i);
		}
	}

	if(vector_pipes == NULL)
	{
		printf("Error a la hora de reservar memoria\n");
	}

	


	//printf("Soy el padre y mi PID es: %d\n",getpid());
	
	for(i=0; i<jugadores; i++)
	{
		pid = fork();

		if(pid == -1)
		{
			printf("Error en la hora de crear el hijo\n");
			exit(-1);	
		}
		
		if (pid == 0)// codigo del hijo todo lo que tengo que hacer con el hijo ba en este if
		{
		    int *combinacion_hijo;
				
			//printf("Soy el hijo %d y mi PID es: %d y mi padre es con PID: %d\n",i,getpid(),getppid());
			apuestaHijo=apuesta();//le paso al hijo su apuesta y la guardo en un puntero a entero
			//printf("==================================================\n");			
			//printf("Soy el Hijo %d y mi apuesta es: ",i);
			//mostrar_apuesta(apuestaHijo); // muestra la apuesta del hijo
			//printf("==================================================\n");
			//le mando la señal al padre de que ya he apostado. y 
			v.sival_int = getpid(); //asignamos valor al dato
			sigqueue(getppid(),SIGRTMIN,v); //enviamos la señal de tiempo real

			combinacion_hijo=hijo_lee(vector_pipes[i]);// en cada posicion le paso un hijo
			
			
			exit(comparar_resultados(combinacion_hijo,apuestaHijo));//funcion que compara lo que a leido el hijo del pipe con su 													Apuesta y devuelve el exit que son los numeros aciertos por parte de cada hijo
					
		}

		else// la primera parte del padre que rellena la estructura de los hijos
		{			
			//que hijo y que PID creo en el momento			
			hijo[i].num = i+1;
			hijo[i].pid = pid;
		}

	}
	

	while(senales_recibidas<jugadores)
	{
		pause(); // el padre espera a que le llegan todas las señales
	}
	
			combinacion=combinacion_ganadora();//le paso al hijo su apuesta y la guardo en un puntero a entero
			//printf("Combinacion ganadora: ");
			//mostrar_combinacion_ganadora(combinacion);

			
			padre_escribe(vector_pipes,jugadores,combinacion);
	
	
			int j, pid_devuelto,salida;
			for(i=0;i<jugadores;i++)//cuando termina el hijo le avisa al padre.
			{
			pid_devuelto=wait(&salida); // wait me devuelve el PID del hijo y el exit de lo que a ganado

					for(j=0; j<jugadores; j++)
					{	
						if(pid_devuelto == hijo[j].pid)
						{		
						 hijo[i].premio=hijo_ganancia(salida>>8);	
						}
					}

			//printf("Jugador %d: Total premio %d Euros. \n",hijo[i].num,hijo[i].premio);
			}	
			generar_fichero(jugadores,sorteos,hijo);
 	
}

