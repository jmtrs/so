#include "funcion.h"
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>


main(int argc, char *argv[]){
     pid_t pid;
     int jugadores;
     int sorteos;
     HIJO *hijo;
     
     jugadores = 6;
     sorteos = 10;
     
     hijo = (HIJO*)malloc(jugadores *sizeof(HIJO)); // reserva de memoria para los hijos
     
    printf("Soy el padre y mi PID es: %d\n",getpid());

    for(int i=0; i<jugadores; i++){
        pid = fork();

        if(pid == -1){ //Error al crear un proceso hijo
             printf("No se ha podido crear el jugador\n");
             printf("Error en la hora de crear el hijo\n");
			 exit(-1);	
        }
        if(pid == 0){ //proceso hijo
             printf("Soy hijo de: %d ..soy.. %d\n", getppid(), getpid());
             exit(0);

            
        }
         
    }
     
}