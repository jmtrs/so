#include "func.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#define LEER 0
#define ESCRIBIR 1

int jugadores_contestados;

main(int argc, char *argv[])
{

int jugadores;
int aciertos[10],ac[10];
int i,j,y;
int ganadoraN[6];
char ganadoraT[18]="";
char T[2];
int apuesta[10][6];
long premios[10];

HIJO hijo[10];

sa.sa_sigaction = handler;   //definición del manejador de la señal de tiempo real 
sa.sa_flags = SA_SIGINFO;
sigaction(SIGRTMIN,&sa,NULL);

int fd[10][2]; //descriptores de archivo para lectura/escritura en el pipe
int pid[10];

//printf("\n\nSORTEO %s\n",argv[1]);

jugadores=atoi(argv[2]);

for(i=0;i<jugadores;i++)
 {
   pipe(fd[i]); //se crea un pipe

   pid[i] = fork(); //el proceso padre crea un proceso hijo

   if (pid[i] == -1) //error al crear el proceso hijo
    {
    printf ("No se ha podido crear el jugador %d\n",i);
    exit(1);
    }
	
   if (pid[i] == 0) //proceso hijo
    {
    generarComb(apuesta[i]);

//for(y=0;y<6;y++) printf("\n apuesta del jugador %i (PID = %d) Nº%i -> %i\n",i+1,getpid(),y+1,apuesta[i][y]);
    
    v.sival_int = getpid(); //asignamos valor al dato para la señal de tiempo real
    sigqueue(getppid(),SIGRTMIN,v); //envia la señal al padre indicando que ha realizado su apuesta 

    close(fd[i][ESCRIBIR]);	
    read(fd[i][LEER], ganadoraT, 18); //lee la combinación ganadora del pipe y si no está se queda esperando
    close(fd[i][LEER]);

    transformarTaN(ganadoraT,ganadoraN); //transforma la combinación leida de texto a número
    aciertos[i]=comprobar(apuesta[i],ganadoraN); //comprueba cuántos números ha acertado
	
//printf ("\n--- Jugador %d terminó de comprobar su apuesta\n",i+1);
	
    exit(aciertos[i]);//informa al padre en su estado determinación de cuantos aciertos ha tenido.
    }

    else //continuación del proceso padre
     {
      hijo[i].pid=pid[i]; 
      hijo[i].num=i+1;
     }

  }

while(jugadores_contestados<jugadores) sleep(1); //el padre espera la confirmación de apuesta de todos los hijos
    
//printf ("--- Todas las puestas realizadas\n");

generarComb(ganadoraN); //el padre genera la combinación ganadora
ordenar(ganadoraN);		
//printf("\nEl padre (PID = %d) genera la combinación ganadora\n\n",getpid());
//for(i=0;i<6;i++) printf(" %d\n",ganadoraN[i]);


for(i=0;i<6;i++)
 {
  sprintf(T, "%d", ganadoraN[i]); ///transforma la combinación ganadora de número a texto
  strcat(ganadoraT,T);
  strcat(ganadoraT," ");
 }

for(i=0;i<jugadores;i++)
 {
  close(fd[i][LEER]);                              
  write(fd[i][ESCRIBIR], ganadoraT, strlen(ganadoraT)); //el padre escribe la combinación ganadora en el pipe
  close(fd[i][ESCRIBIR]); 
 }

for(i=0;i<jugadores;i++) 
 {
  waitpid(pid[i],&aciertos[i],0);    //el padre espera la terminación de todos los hijos
  ac[i] = WEXITSTATUS(aciertos[i]);

  hijo[i].aciertos=ac[i];
  hijo[i].premio=premio(ac[i]);
  premios[i]=hijo[i].premio;
 }

fichero(argv[1],jugadores,premios); //el padre genera el fichero de resultados
    
//printf("\n- Resultados del SORTEO %s\n",argv[1]);
//for(i=0;i<jugadores;i++) printf("Jugador %d (PID= %d), Aciertos = %d, Premio = %ld €\n",hijo[i].num,hijo[i].pid,hijo[i].aciertos,hijo[i].premio);

exit(1);
}
