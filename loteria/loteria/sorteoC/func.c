#include <stdio.h>
#include <string.h>
#include <signal.h>

extern int jugadores_contestados;

void generarComb(int *c)
{
int i,j;
srand (getpid());
 
for(i=0;i<6;i++) 
 {             
 c[i] = rand() %15+1;
 for(j=0;j<6;j++) if(c[i]==c[j] && i!=j) i--;
 }
}

void ordenar(int *g)
{
int i,j,cajon;

for (i=0;i<6;i++)
 for (j=0;j<5;j++)
  if (g[j]>g[j+1]) 
   {
   cajon=g[j];
   g[j]=g[j+1];
   g[j+1]=cajon;
   }
}

int comprobar(int ap[], int gan[])
{
int j,k,ac=0;

 for(j=0;j<6;j++)
  for(k=0;k<6;k++)
   if(ap[j]==gan[k]) ac++;

return ac;
}

long premio(int a)
{
long p;
	
 if(a==6) p=10000; 
 else if (a==5) p=500;
 else if (a==4) p=50;
 else if (a==3) p=10;
 else p=0;

return p;
}

void fichero(char Ns[], int jug, long p[])
{
FILE *f;
int i;
char s[5]="S";

strcat(s,Ns);
strcat(s,"R");

f = fopen (s,"w");

if(f==NULL) printf("\n* ERROR: No se pudo crear el fichero '%s'\n",s);
else
 {
 fprintf(f,"#Resultados Sorteo %s\n",Ns);

 for(i=0;i<jug;i++)
  fprintf(f,"%li\n",p[i]);
 }

if(fclose(f)) printf( "\n* ERROR: Fichero '%s' NO CERRADO\n",s);
}

void transformarTaN (char ganT[], int *ganN)
{
int i,j=0,k=0;
char T[2];

 for(i=0;i<strlen(ganT);i++)
  {
  if(ganT[i]!=' ')
   {
    T[k]=ganT[i];
    k++;
   }

   else
    {
     ganN[j]=atoi(T);
     j++;
     k=0;
    }
  }
}

void handler (int signo, siginfo_t *info, void *other) 
{
//printf("\n\n--- Señal de apuesta terminada del hijo %d\n\n",info->si_value.sival_int);
jugadores_contestados++;
}


