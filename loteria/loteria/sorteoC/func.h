#include <signal.h>

void generarComb(int *);
void ordenar(int *);
int comprobar(int [], int []);
long premio(int);
void fichero(char [], int, long []);
void transformarTaN (char [], int *);
void handler (int, siginfo_t *, void *);

typedef struct
{
int pid;
int num;
int aciertos;
long premio;
} HIJO;

struct sigaction sa;
union sigval v;
